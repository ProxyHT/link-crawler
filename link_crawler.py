import sys
import utils.crawler as crawler
from cfg.config import SOURCE

def get_links(keyword="",limit=100):
    final_links = []

    for idx,source in enumerate(SOURCE):
        remaining_count = limit - len(final_links)
        source_limit = int(remaining_count/(len(SOURCE)-idx))
        print("Fetching for {} links from {}".format(source_limit,source))
        links = crawler.crawl_link(source=source,keyword=keyword,limit=source_limit)
        print("Found {} links".format(len(links)))
        final_links += links
    
    return final_links

if __name__ == "__main__":
    import time
    now = time.time()

    keyword = sys.argv[1]
    limit = int(sys.argv[2])

    links = get_links(keyword=keyword,limit=limit)
    print("FETCHED {} links. Time taken {}".format(len(links),time.time()-now))