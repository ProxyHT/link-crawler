CHROME_TYPE = "remote" # remote|driver (use remote driver | use local executable driver)
CHROME_PATH = "http://127.0.0.1:4444/wd/hub"

# there are 3 types of website pagination: static page, lazyload page (with loadmore button) and js pagination (client side rendering)
# "static" | "lazyload" | "csr"
SOURCE = {
    "https://vnexpress.net": {
        "scrap_url": "https://timkiem.vnexpress.net/?q={$query$}&page={$page$}",
        "type": "static",
        "url_xpath": "//article//h3[@class='title-news']//a"
    },
    "https://dantri.com": {
        "scrap_url": "https://dantri.com.vn/tim-kiem.htm?keywords={$query$}",
        "type": "csr",
        "url_container_xpath": "//div[@class='gsc-results gsc-webResult']",
        "url_xpath": "//a[@class='gs-title']",
        "pagination_xpath": "//div[@class='gsc-cursor-page' and normalize-space()='{$page$}']"
    },
    "https://kenh14.vn": {
        "scrap_url": "http://search.kenh14.vn/?query={$query$}",
        "type": "lazyload",
        "url_xpath": "//h3[@class='knswli-title']//a",
        "loadmore_xpath": "//div[@class='view-more-detail clearboth']//a"
    }
}