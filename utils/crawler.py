import time
from cfg.config import CHROME_TYPE,CHROME_PATH,SOURCE
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By

wd_options = Options()
wd_options.add_argument("--headless")
wd_options.add_argument('--no-sandbox')
wd_options.add_argument('--disable-dev-shm-usage')

def crawl_link(source="",keyword="",limit=100):
    if CHROME_TYPE == "remote":
        wd = webdriver.Remote(CHROME_PATH, DesiredCapabilities.CHROME,options=wd_options)
    elif CHROME_TYPE == "driver":
        wd = webdriver.Chrome(CRHOME_PATH, options=wd_options)
    else:
        return Exception("Invalid driver type")

    try:
        config = SOURCE[source]
    except:
        print("Error loading source config")
        return Exception("Invalid source config")

    links = []

    page = 1
    count_post = 0
    try: 
        if config["type"] == "static":
            while True:
                request_url = config["scrap_url"].replace("{$query$}",keyword).replace("{$page$}",str(page))
                wd.get(request_url)
                post_link_elements = wd.find_elements_by_xpath(config["url_xpath"])
                post_links = [element.get_attribute("href") for element in post_link_elements]
                links += post_links
                page += 1
                count_post += len(post_links)
                if (count_post > limit):
                    break

        elif config["type"] == "csr":
            request_url = config["scrap_url"].replace("{$query$}",keyword)
            wd.get(request_url)
            while True:
                WebDriverWait(wd, 10).until(ec.visibility_of_element_located((By.XPATH, config["url_container_xpath"])))
                post_link_elements = wd.find_elements_by_xpath(config["url_xpath"])
                post_links = [element.get_attribute("href") for element in post_link_elements]
                
                links += post_links
                page += 1
                count_post += len(post_links)
                if (count_post > limit):
                    break
                WebDriverWait(wd, 10).until(ec.visibility_of_element_located((By.XPATH, config["pagination_xpath"].replace("{$page$}",str(page))))).click()

        elif config["type"] == "lazyload":
            request_url = config["scrap_url"].replace("{$query$}",keyword)
            wd.get(request_url)
            while True:
                """INFINITE SCROLLING"""
                SCROLL_PAUSE_TIME = 2

                # Get scroll height
                last_height = wd.execute_script("return document.body.scrollHeight")

                while True:
                    # Scroll down to bottom
                    wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                    # Wait to load page
                    time.sleep(SCROLL_PAUSE_TIME)

                    # Calculate new scroll height and compare with last scroll height
                    new_height = wd.execute_script("return document.body.scrollHeight")
                    if new_height == last_height:
                        break
                    last_height = new_height
                """"""

                post_link_elements = wd.find_elements_by_xpath(config["url_xpath"])
                post_links = [element.get_attribute("href") for element in post_link_elements]
                
                links = post_links
                count_post = len(post_links)
                if (count_post > limit):
                    break
                
                loadmore_elem = WebDriverWait(wd, 10).until(ec.element_to_be_clickable((By.XPATH, config["loadmore_xpath"])))
                wd.execute_script("arguments[0].click();", loadmore_elem)
        else: 
            print("Error: No type specified")
    except:
        pass

    wd.close()

    return links[:limit]
