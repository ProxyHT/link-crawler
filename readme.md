# KEYWORD CRAWLER

Post link crawler by keyword

## Getting Started

### Technologies

1. Python
2. Headless browser selenium

### Prerequisites

1. Docker version 19.03.8
2. docker-compose version 1.25.4
3. Python 3 

### Installing

A step by step series of examples that tell you how to get a development env running

Set up the environment with docker-compose

```
docker-compose up -d
```

Install python packages

```
pip install -r requirements.txt
```

Advance configuration in `./cfg/config.py`


## Running the crawler

Run the crawler by executing


```
python ./link_crawler.py {KEYWORD} {LIMIT}
```

Eg: python ./link_crawler.py "vui" 100


## Customize the crawler

Customize the crawler in ```link_crawler.py```
```
get_links(keyword:string,limit:int)
```

```keyword```: ```string``` - The keyword used for search

```limit```: ```int``` - The limit links to be crawled



## Author
proxyht
